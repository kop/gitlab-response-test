package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/cheggaaa/pb"
)

// StatsStorer is an interface expected by makeRequest function.
type StatsStorer interface {
	Success(duration time.Duration)
	Failure()
}

// StatsStorage is a storage used to keep test data.
type StatsStorage struct {
	sync.RWMutex
	Total              uint          // Total number of requests performed
	Successful         uint          // Number of successful requests
	Failures           uint          // Number of failed requests
	Latencies          time.Duration // A sum of latencies of all successful requests
	NetConnOpenedCount uint          // A number of times TCP connection was opened
	NetConnClosedCount uint          // A number of times TCP connection was closed
}

// Success writes successful result to the storage.
func (storage *StatsStorage) Success(duration time.Duration) {

	storage.Lock()
	defer storage.Unlock()

	storage.Total++
	storage.Successful++
	storage.Latencies += duration
}

// Failure writes failure result to the storage.
func (storage *StatsStorage) Failure() {

	storage.Lock()
	defer storage.Unlock()

	storage.Total++
	storage.Failures++
}

// AverageDuration calculates average duration in Delays slice.
func (storage *StatsStorage) AverageDuration() (average time.Duration) {

	storage.RLock()
	defer storage.RUnlock()

	if storage.Successful == 0 {
		return
	}

	average = time.Duration(int64(storage.Latencies.Nanoseconds()) / int64(storage.Successful))
	return
}

// ConnOpened increments a counter of opened TCP connections.
func (storage *StatsStorage) NetConnOpened() {

	storage.Lock()
	defer storage.Unlock()

	storage.NetConnOpenedCount++
}

// ConnClosed increments a counter of closed TCP connections.
func (storage *StatsStorage) NetConnClosed() {

	storage.Lock()
	defer storage.Unlock()

	storage.NetConnClosedCount++
}

// onCloseNetConn is a wrapper around net.Conn that calls a custom callback when TCP connection is closed.
type onCloseNetConn struct {
	net.Conn
	onClose func()
}

// Close closes the connection.
func (closer onCloseNetConn) Close() error {
	closer.onClose()
	return closer.Conn.Close()
}

// main starts program execution
func main() {

	testDuration := 300 // how long to run a test (in sec)
	testRps := 1        // number of requests to make per second
	testIterations := testDuration * testRps

	// Show progress bar
	fmt.Printf("Starting GitLab connectivity test (duration: %ds, RPS: %d)...\n", testDuration, testRps)
	bar := pb.New(testIterations)
	bar.ShowSpeed = false
	bar.Start()

	// Prepare the storage
	storage := &StatsStorage{}

	// Prepare HTTP client
	transport := &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			conn, err := net.Dial(network, addr)
			if err != nil {
				return conn, err
			}
			storage.NetConnOpened()
			return onCloseNetConn{conn, func() { storage.NetConnClosed() }}, err
		},
	}
	client := &http.Client{
		Transport: transport,
		CheckRedirect: func(req *http.Request, via []*http.Request) (err error) {
			return http.ErrUseLastResponse
		},
	}

	// Start the loop
	sleepDuration := time.Second / time.Duration(testRps)
	for storage.Total < uint(testIterations) {
		go func() {
			makeRequest(client, storage)
			bar.Increment()
		}()
		time.Sleep(sleepDuration)
	}

	// Print results
	bar.Finish()
	fmt.Print("Test results are:\n")
	fmt.Printf("- total requests made: %d\n", storage.Total)
	fmt.Printf("- successful requests: %d\n", storage.Successful)
	fmt.Printf("- failed requests: %d\n", storage.Failures)
	fmt.Printf("- average request duration: %v\n", storage.AverageDuration())
	fmt.Printf("- TCP connection opened: %d times\n", storage.NetConnOpenedCount)
	fmt.Printf("- TCP connection closed: %d times\n", storage.NetConnClosedCount)
}

// makeRequest is responsible for making an HTTP request to gitlab.com and for writing result to the stats storage.
func makeRequest(client *http.Client, storer StatsStorer) {

	timeOne := time.Now()

	response, err := client.Get("https://gitlab.com")
	if err != nil {
		storer.Failure()
		return
	}
	io.Copy(ioutil.Discard, response.Body)
	response.Body.Close()

	timeTwo := time.Now()
	storer.Success(timeTwo.Sub(timeOne))
}
