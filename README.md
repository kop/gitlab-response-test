GitLab Response Tester
======================

This is a little tool that measures GitLab HTTP responsibility.


Usage
-----

Simply run a binary.

This tool has a hardcoded settings of running 1 request per second during 5 minutes time period.

On completion, it will give you some stats, including an average request duration.


Building
--------

You will need a [Glide](https://glide.sh/) to build this project:

```bash
cd ./gitlab-response-test
glide install
go build
```